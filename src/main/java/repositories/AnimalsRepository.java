package repositories;

import domain.models.Animals;
import repositories.interfaces.IDBRepository;
import repositories.interfaces.IEntityRepository;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.ServerErrorException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

public class AnimalsRepository implements IEntityRepository<Animals> {

    private IDBRepository dbrepo;

    public AnimalsRepository() {
        dbrepo =  new PostgresRepository();
    }

    @Override
    public void add(Animals entity) {
            try {
                String sql = "INCERT INTO animals(nickname, regid, type, breed, age)"+
                        "VALUES(?, ?, ?, ?, ?, ?)";

                PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
                stmt.setString(1, entity.getNickname());
                stmt.setString(2, entity.getRegid());
                stmt.setString(3, entity.getType());
                stmt.setString(4, entity.getBreed());
                stmt.setInt(5, entity.getAge());
                stmt.execute();
            } catch (SQLException ex) {
                throw new ServerErrorException("Can not run SQL", 500);
            }
    }

    @Override
    public void update(Animals entity) {
        String sql = "UPDATE owners " +
                "SET ";
        int c = 0;
        if (entity.getNickname() != null) {
            sql += "nickname=?, "; c++;
        }
        if (entity.getRegid() != null) {
            sql += "regid=?, "; c++;
        }

        if (entity.getType() != null) {
            sql += "type=?, "; c++;
        }
        if (entity.getBreed() != null) {
            sql += "breed=?, "; c++;
        }


        sql = sql.substring(0, sql.length() - 2);

        sql += " WHERE id = ?";

        try {
            int i = 1;
            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            if (entity.getNickname() != null) {
                stmt.setString(i++, entity.getNickname());
            }
            if (entity.getType() != null) {
                stmt.setString(i++, entity.getType());
            }
            if (entity.getRegid() != null) {
                stmt.setString(i++, entity.getRegid());
            }

            stmt.setInt(i++, entity.getId());

            stmt.execute();
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
    }


    @Override
    public void remove(Animals owner) {

    }

    @Override
    public List<Animals> query(String sql) {
        try {
            Statement stmt = dbrepo.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            LinkedList<Animals> ls = new LinkedList<>();
            while (rs.next()) {
                Animals a = new Animals(
                        rs.getInt("id"),
                        rs.getString("nickname"),
                        rs.getString("regid"),
                        rs.getString("type"),
                        rs.getString("breed"),
                        rs.getInt("age")
                );
                ls.add(a);

            }
            return ls;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public Animals queryOne(String sql) {
        return null;
    }
}
