package domain.models;
// it is a superclass of herd and pets
public class Animals {
    private int id = -1;
    private String nickname;
    private String regid;
    private String type;
    private String breed;
    private int age;

    public Animals() {

    }


    public Animals(String nickname, String regid, String type, String breed, int age) {
        setNickname(nickname);
        setRegid(regid);
        setType(type);
        setBreed(breed);
        setAge(age);
    }

    public Animals(int id, String nickname, String regid, String type, String breed, int age) {
        setId(id);
        setNickname(nickname);
        setRegid(regid);
        setType(type);
        setBreed(breed);
        setAge(age);
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getRegid() {
        return regid;
    }

    public void setRegid(String regid) {
        this.regid = regid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return id + " " + nickname + " " +  regid+ " " +  type + " " + breed + " " + age ;
    }
}
