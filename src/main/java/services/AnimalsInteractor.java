package services;

import domain.models.User;
import repositories.AnimalsRepository;
import repositories.UserRepository;
import repositories.interfaces.IEntityRepository;

public class AnimalsInteractor {
    private IEntityRepository animalsRepo;
    private IEntityRepository ownerRepo;

    public AnimalsInteractor() {
        animalsRepo = new AnimalsRepository();
        ownerRepo = new UserRepository();
    }

    public Iterable<User> getAnimalsOwners(int id) {
         Iterable<User> list = ownerRepo.query("Select o.* ,a.*  from owners_animals_mapping m, owners o, animals a\n" +
                 "where m.id_owners = o.id and m.id_animals = a.id and o.id = " + id);
         return list;
    }

}
